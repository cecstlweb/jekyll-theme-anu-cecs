---
layout: none
---

{% if site.piwik_siteid %}
<!-- Piwik -->
var _paq = _paq || [];
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
  var u="//cecs.anu.edu.au/analytics/";
  _paq.push(['setTrackerUrl', u+'piwik.php']);
  _paq.push(['setSiteId', '{{ site.piwik_siteid }}']);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
  g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
})();
<!-- End Piwik Code -->
{% endif %}

function updateTabs(tabgroup, hash) {
  jQuery(".tabcontent-" + tabgroup).addClass("hidden");
  jQuery(".tab-" + tabgroup).removeClass("pagetabs-select");
  jQuery("#" + hash).addClass("pagetabs-select");
  jQuery("#tabcontent-" + hash).removeClass("hidden");

  jQuery(".tab-" + tabgroup).addClass("noprint");
  jQuery("#" + hash).removeClass("noprint");
}

function updateTabPage(tabgroup) {
  return function () {
    var istab = window.location.hash.substring(1, tabgroup.length + 1);
    if(istab == tabgroup) {
      var hash = window.location.hash.substring(1);
      updateTabs(tabgroup, hash)
    }
  }
}

function toggleSmallMenu() {
  slideDownFadeOutToggle('#menudropdown');
  slideDownFadeOutToggle('#menu');
}

jQuery(document).ready(function() {
  if (window.innerWidth < 960) {
    hideID('#menudropdown');
    hideID('#menu');
  }
  {% if site.gitlab_auth == true %}
  jQuery.get("/pages/auth/status", function(data) {
    jQuery("#bnr-low").html(data);
  });
  {% endif %}

if (!$anujq("html").hasClass("ie6") && !$anujq("html").hasClass("ie7") && !$anujq("html").hasClass("ie8") && !$anujq("html").hasClass("ie9")) {
    $anujq(window).resize(function() {
        newpagewidth = window.innerWidth;
        if (pagewidth < 960 && newpagewidth >= 960) {
            changeUpTo960();
        } else if (pagewidth >= 960 && newpagewidth < 960) {
            changeDownTo600();
        }
        pagewidth = newpagewidth;
    });
}
if(window.innerWidth >=960) {
  showID('#menu');
}

$anujq("a.tab").on('click', function (e) {
  e.preventDefault();
  history.pushState({}, '', this.href);
  var href = $anujq(this).attr('href');
  var tabgroup = href.substring(1, href.length - 1);
  var hash = href.substring(1);
  updateTabs(tabgroup, hash)
});

});

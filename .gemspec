# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-anu-cecs"
  spec.version       = "1.3.10"
  spec.authors       = ["David Guest", "Ben Swift"]
  spec.email         = ["helpdesk@cecs.anu.edu.au"]

  spec.summary       = "ANU CECS Theme for Jekyll Pages with gitlab"
  spec.homepage      = ""
  spec.license       = "Nonstandard"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|css|js|LICENSE|README|feed.xml|search.json)}i) }

  spec.add_runtime_dependency "jekyll", "~> 3.8"

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"

end

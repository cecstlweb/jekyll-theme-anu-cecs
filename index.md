---
title: Home
hidden: true
hide_heading: true
tags: home
---


<h2 class="nounderline">Undergradate students</h2>
<div class="anutoggle">
  <p>Content for undergraduate students.</p>
</div>


<h2 class="nounderline">Undergradate students</h2>
<div class="anutoggle">
  <p>Content for undergraduate students.</p>
</div>

{% assign tabcontent2 = "" | split: ""  %}
{% capture data %}
 * This is a list
 * More list
{% endcapture %}
{% assign tabcontent2 = tabcontent2 | push: data  %}

{% capture data %}
```
 This is some code
 ```
{% endcapture %}
{% assign tabcontent2 = tabcontent2 | push: data  %}

{% capture data %}
## This is a heading
{% endcapture %}
{% assign tabcontent2 = tabcontent2 | push: data %}
{% assign tabs = "section3,section4,section5" | split: "," %}

{% include tabs tabgroup="group2" tabs=tabs tabcontent=tabcontent2 %}
